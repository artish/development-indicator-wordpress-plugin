<?php
/*
Plugin Name: Development Environment Indicator
Description: Adds a custom FavIcon and a Blue Bar at the top of your page to indicate
             that you're in an development environment.
*/

function devenv_favicon_insert() {
    $favicon_path = plugins_url( '/devenv-indicator-mod-favicon.png', __FILE__ );    
    $favicon = '<link rel="shortcut icon" href="' . $favicon_path . '" />';
    echo $favicon;
}

function devenv_dev_bar_insert() {
  $style = 'display: block; height: 2px; background: #44AFF1; width: 100%; position: fixed; top: 0; left: 0; z-index: 999999999';
  $element = '<div style="' . $style . '"></div>';
  echo $element;
}

// Add the Favicon
add_action( 'wp_head', 'mod_favicon_insert' );
add_action( 'admin_head', 'mod_favicon_insert' );

// Add the blue Dev Bar
add_action( 'wp_footer', 'mod_dev_bar_insert', 999 );
add_action( 'admin_footer', 'mod_dev_bar_insert', 999 );

?>